#include <stdio.h>
#include <stdlib.h>
#include "swiatlib.h"

const int DOMYSLNA_WAGA_NIEB = 77;
const int DOMYSLNA_WAGA_ZIEL = 151;
const int DOMYSLNA_WAGA_CZER = 28;

extern int swiat_w_odcieniach_szarosci(char *obraz, int dlugosc, int szerokosc);
extern int NIEB_UZY;
extern int CZER_UZY;
extern int ZIEL_UZY;

int main(int argc, char *argv[])
{
	int szer, wys, maxkol;
	char *wynik;
	char *domyslne_wejscie = "test.ppm";
	char *domyslne_wyjscie = "wynik.pgm";
	if (argc == 1 || argc > 6) {
		fprintf(stderr, "Sposob uzycia:\n"
			"\t-czyta wejscie z pliku %s, wyjscie do pliku %s gdy"
			"nie podane sa zadne argumenty z wiersza polecen\n"
			"\t-gdy jest podany pierwszy argument z wiersza polecen"
			" traktuje go jako plik z wejsciem\n"
			"\t-gdy sa podane dwa argumenty z wiersza polecen to "
			"drugi jest traktowany jako plik, do ktorego piszemy x"
			"wynik\n", domyslne_wejscie, domyslne_wyjscie);
		fprintf(stderr, "jako 4, 5 i 6 parametr mozna podac kolejno "
			"wage koloru czerwonego, zielonego, niebieskiego.\n"
			"Powinny byc nieujemnymi liczbami i sumowac sie do 256."
			"\nPoprawnosc tych liczb nie jest sprawdzana.\n");
	}
	if (argc > 6)
		return EXIT_SUCCESS;
	if (argc >= 2) {
		fprintf(stderr, "Czytam wejscie z pliku %s\n", argv[1]);
		wynik = wczytaj_plik(argv[1], &szer, &wys, &maxkol);
	} else {
		fprintf(stderr, "Nie podano pliku z wejsciem z linii polecen, "
			"wiec otwieram plik %s\n", domyslne_wejscie);
		wynik = wczytaj_plik(domyslne_wejscie, &szer, &wys, &maxkol);
	}
	if (argc == 6) {
		CZER_UZY = atoi(argv[3]);
		ZIEL_UZY = atoi(argv[4]);
		NIEB_UZY = atoi(argv[5]);
	} else {
		NIEB_UZY = DOMYSLNA_WAGA_NIEB;
		ZIEL_UZY = DOMYSLNA_WAGA_ZIEL;
		CZER_UZY = DOMYSLNA_WAGA_CZER;
	}
	swiat_w_odcieniach_szarosci(wynik, wys, szer);
	if (argc != 3 && argc != 6) {
		fprintf(stderr, "Wynik zapisuje w pliku %s\n",
			domyslne_wyjscie);
		stworz_plik_i_wypisz(domyslne_wyjscie, szer, wys, maxkol,
				     wynik);
	} else {
		fprintf(stderr, "Wynik zapisuje w pliku %s\n", argv[2]);
		stworz_plik_i_wypisz(argv[2], szer, wys, maxkol, wynik);
	}
	return EXIT_SUCCESS;
}
