LIBS=swiat-w-odcieniach-szarosci.o swiatlib.o err.o
ASM=as
LD=gcc
CC=gcc
CFLAGS=-std=gnu99 -g -Wall -Wextra
LDFLAGS=-g -Wall -Wextra

all: test

test: test.o $(LIBS)
	$(LD) -o $@ $^ $(LDFLAGS)

swiat-w-odcieniach-szarosci.o: swiat-w-odcieniach-szarosci.gas
	$(ASM) -g -o $@ $^

clean:
	rm -f *.o *~ test

scp:
	make clean
	scp -P 5555 * user@localhost:~/

wez:
	scp -P 5555 user@localhost:~/wynik.pgm .

tests: test
	./test 2052.ppm 2052.pgm 0 0 256
	./test clouds.ppm clouds.pgm 80 80 96
	./test dirt.ppm dirt.pgm 
	./test teapot.ppm teapot.pgm 50 50 156
	./test green-apple.ppm green-apple.pgm
	./test GroundGrass.ppm GroundGrass.pgm
