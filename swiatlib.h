#ifndef SWIAT_W_ODCIENIACH_SZAROSCI_LIB_H
#define SWIAT_W_ODCIENIACH_SZAROSCI_LIB_H

/* nazwa_pliku to parametr wejsciowy, szerokosc i dlugosc to tez wyniki */
extern char *wczytaj_plik(const char *const nazwa_pliku, int *const szerokosc,
			  int *const wysokosc, int *const max_wart_koloru);

extern void stworz_plik_i_wypisz(char *const nazwa_pliku,
				 const int szerokosc, const int wysokosc,
				 const int MAX_WARTOSC,
				 char *dane);

#endif
