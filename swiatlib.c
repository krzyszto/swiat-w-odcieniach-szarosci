#include "swiatlib.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "err.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum stan_czytania_wejscia {
	KOMENTARZ,
	BIALE_ZNAKI,
	CYFRA,
};

int otworz_plik(const char *const nazwa_pliku)
{
	int fd = open(nazwa_pliku, O_RDONLY);
	return fd;
}

void wczytaj_i_sprawdz_magiczna_liczbe(const int fd)
{
	const size_t DLUGOSC_MAGICZNEJ_LICZBY = 3;
	char mn[DLUGOSC_MAGICZNEJ_LICZBY + 1];
	ssize_t wynik;
	wynik = read(fd, mn, DLUGOSC_MAGICZNEJ_LICZBY);
	if (wynik != (ssize_t) DLUGOSC_MAGICZNEJ_LICZBY)
		syserr("Nie udało się przeczytać magicznej liczby.");
	mn[DLUGOSC_MAGICZNEJ_LICZBY] = '\0';
	if (mn[0] != 'P' || mn[1] != '6' || (mn[2] != ' ' && mn[2] != '\n')) {
		fatal("Nie zgadza sie magiczna liczba, oczekiwalem 'P6'"
		      " a dostalem %s", mn);
	}
}

int znajdz_pierwsza_cyfre(const int fd)
{
	int wynik_read;
	char tmp;
	const size_t ROZMIAR_TMP = 1;
	enum stan_czytania_wejscia wyn = BIALE_ZNAKI;
	tmp = '\n';
	do {
		wynik_read = read(fd, &tmp, ROZMIAR_TMP);
		switch (wyn) {
		case KOMENTARZ:
			if (tmp == '\n')
				wyn = BIALE_ZNAKI;
			break;
		case BIALE_ZNAKI:
			if (strchr(" \n", tmp) != NULL)
				break;
			if (tmp - '0' > 0 && tmp - '0' <= 9) {
				wyn = CYFRA;
				break;
			}
			if (tmp == '#') {
				wyn = KOMENTARZ;
				break;
			}
			fatal("Szukalem cyfry, a znalazlem %c.", tmp);
		default:
			fatal("nie powinienem tu byc.");
		}
	} while (wynik_read == (ssize_t) ROZMIAR_TMP && wyn != CYFRA);
	if (wynik_read != (ssize_t) ROZMIAR_TMP) {
		syserr("Nie udało się wczytac pliku, szukalem cyfry.");
	}
	return tmp - '0';
}

void wczytaj_reszte(const int fd, int *const poczatek)
{
	char tmp;
	const size_t ROZMIAR_BUFORA = 1;
	ssize_t wynik = read(fd, &tmp, ROZMIAR_BUFORA);
	while (wynik == (ssize_t) ROZMIAR_BUFORA && tmp >= '0' && tmp <= '9') {
		*poczatek *= 10;
		*poczatek += tmp - '0';
		read(fd, &tmp, ROZMIAR_BUFORA);
	}
	if (wynik != (ssize_t) ROZMIAR_BUFORA) {
		syserr("Nie udało się wczytać liczby z pliku.");
	}
	if (strchr(" \n", tmp) == NULL) {
		fatal("Spodziewalem sie bialego znaku na koniec liczby,"
		      " a spotkalem %c", tmp);
	}
}

int wczytaj_liczbe(const int fd)
{
	int poczatek = znajdz_pierwsza_cyfre(fd);
	wczytaj_reszte(fd, &poczatek);
	return poczatek;
}

char *wczytaj_dane(const int fd, int rozmiar_danych)
{
	const size_t MAX_CZYTANIE_NA_RAZ = 7423;
	/* O 1 wiecej alokujemy, zeby potem moc oszukiwac w asm. */
	char *wynik = malloc(1 + sizeof(char) * rozmiar_danych);
	char *wynik_tmp = wynik;
	ssize_t wynik_read;
	if (wynik == NULL)
		syserr("Nie ma pamięci na wczytanie wejscia.");
	while (rozmiar_danych > (ssize_t) MAX_CZYTANIE_NA_RAZ) {
		wynik_read = read(fd, wynik_tmp, MAX_CZYTANIE_NA_RAZ);
		wynik_tmp += MAX_CZYTANIE_NA_RAZ;
		if (wynik_read != (ssize_t) MAX_CZYTANIE_NA_RAZ) {
			syserr("Nie udało się wczytać danych binarnych.");
		}
		rozmiar_danych -= MAX_CZYTANIE_NA_RAZ;
	}
	/* Read 0 bajtow ma jakies problemy. */
	if (rozmiar_danych == 0)
		return wynik;
	wynik_read = read(fd, wynik_tmp, rozmiar_danych);
	if (wynik_read != (ssize_t) rozmiar_danych) {
		syserr("Nie udało się wczytać danych binarnych.");
	}
	return wynik;
}

char *wczytaj_plik(const char *const nazwa_pliku, int *const szerokosc,
		   int *const wysokosc, int *const max_wart_koloru)
{
	int fd = open(nazwa_pliku, O_RDONLY);
	int rozmiar_danych;
	char *wynik;
	if (fd == -1) {
		syserr("Nie można otworzyć pliku %s z wejściem.", nazwa_pliku);
	}
	wczytaj_i_sprawdz_magiczna_liczbe(fd);
	*szerokosc = wczytaj_liczbe(fd);
	if (*szerokosc <= 0) {
		fatal("Nie zgadza sie szerokosc, powinno byc wiecej niz 0, "
		      "a jest %i", *szerokosc);
	}
	*wysokosc = wczytaj_liczbe(fd);
	if (*wysokosc <= 0) {
		fatal("Nie zgadza sie wysokosc, powinno byc wiecej niz 0, "
		      "a jest %i", *wysokosc);
	}
	*max_wart_koloru = wczytaj_liczbe(fd);
	if (*max_wart_koloru <= 0 || *max_wart_koloru > 256) {
		fatal("Nie zgadza sie max wartosc koloru, "
		      "powinno byc nie wiecej niz 256 i nie mniej niz 1, "
		      "dostalismy %i.", *max_wart_koloru);
	}
	rozmiar_danych = 3 * (*wysokosc) * (*szerokosc);
	wynik = wczytaj_dane(fd, rozmiar_danych);
	if (close(fd) != 0)
		syserr("Nie udało się zamknąć deskryptora.");
	return wynik;
}

void stworz_plik_i_wypisz(char *const nazwa_pliku,
			  const int szerokosc,
			  const int wysokosc,
			  const int MAX_WARTOSC,
			  char *dane)
{
	char *MAGICZNA_LICZBA = "P5";
	FILE* wyjscie = fopen(nazwa_pliku, "w");
	if (wyjscie == NULL)
		syserr("Nie udalo sie otworzyc pliku do zapisu wyjscia.");
	fprintf(wyjscie, "%s\n%i %i %i\n", MAGICZNA_LICZBA, szerokosc, wysokosc,
		MAX_WARTOSC);
	for (int i = 0; i < szerokosc * wysokosc; ++i) {
		fprintf(wyjscie, "%c", *dane);
		++dane;
	}
	fflush(wyjscie);
	fclose(wyjscie);
}
